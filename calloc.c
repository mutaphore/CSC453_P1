#include "malloc.h"

void *calloc(size_t nmemb, size_t size) {
   Header *header;
   void *rtn;

   if (rtn = malloc(nmemb * size)) {
      header = (Header *)((char *)rtn - sizeof(Header));
      memset(rtn, 0, header->size);   //Set to all 0's
      if (getenv("DEBUG_MALLOC")) {
         printf("MALLOC: calloc(%lu,%lu)  =>  (ptr=%p, size=%lu)\n",
          nmemb, size, rtn, ((Header *)((char *)rtn - sizeof(Header)))->size);
      }
   }

   return rtn;
}
