CFLAGS = -g

intel-all: lib/libmalloc.so lib64/libmalloc.so

lib/libmalloc.so: lib malloc32.o free32.o realloc32.o calloc32.o
	gcc $(CFLAGS) -fpic -m32 -shared -o $@ malloc32.o free32.o realloc32.o calloc32.o

lib64/libmalloc.so: lib64 malloc64.o free64.o realloc64.o calloc64.o
	gcc $(CFLAGS) -fpic -shared -o $@ malloc64.o free64.o realloc64.o calloc64.o

lib:
	mkdir lib

lib64:
	mkdir lib64

malloc32.o: malloc.c malloc.h
	gcc $(CFLAGS) -fpic -m32 -c -o malloc32.o malloc.c

malloc64.o: malloc.c malloc.h
	gcc $(CFLAGS) -fpic -m64 -c -o malloc64.o malloc.c

free32.o: free.c malloc.h
	gcc $(CFLAGS) -fpic -m32 -c -o free32.o free.c

free64.o: free.c malloc.h
	gcc $(CFLAGS) -fpic -m64 -c -o free64.o free.c

realloc32.o: realloc.c malloc.h
	gcc $(CFLAGS) -fpic -m32 -c -o realloc32.o realloc.c

realloc64.o: realloc.c malloc.h
	gcc $(CFLAGS) -fpic -m64 -c -o realloc64.o realloc.c

calloc32.o: calloc.c malloc.h
	gcc $(CFLAGS) -fpic -m32 -c -o calloc32.o calloc.c

calloc64.o: calloc.c malloc.h
	gcc $(CFLAGS) -fpic -m64 -c -o calloc64.o calloc.c

clean:
	rm -f *.o *.a
