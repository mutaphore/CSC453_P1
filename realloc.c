#include "malloc.h"

void *realloc(void *ptr, size_t size) {
   Header *header = (Header *)((char *)ptr - sizeof(Header)), *temp;
   void *rtn = ptr;
   size_t inputSize = size;

   //Check to see if size is divisible by 16
   if (size % BOUNDARY)
      size = (size / BOUNDARY + 1) * BOUNDARY;

   if (ptr) {
      if (header->size > size) {   //Smaller case
         if (size == 0)
            free(ptr);
         else
            Parcel(header, size);
      }
      else if (header->size < size) {   //Larger case
         //Check for to see if adjacent block is free and large enough
         if (header->next && header->next->isFree &&
          header->size + header->next->size + sizeof(Header) >= size) {
            header->size += header->next->size + sizeof(Header);
            header->next = header->next->next;
            Parcel(header, size);
         }
         else {
            //Traverse linked list to find a spot to copy to
            if (temp = FindFree(size)) {
               rtn = temp + 1;
               memcpy(rtn, header + 1, header->size);
               header->isFree = 1;
            }
            else
               rtn = NULL;   //Unable to allocate new space, return NULL
         }
      }
   }
   else   //if ptr is NULL same as malloc(size)
      rtn = malloc(size);

   if (getenv("DEBUG_MALLOC")) {
      printf("MALLOC: realloc(%p,%lu) =>  (ptr=%p,size=%lu)\n",
       ptr, inputSize, rtn,
        ((Header *)((char *)rtn - sizeof(Header)))->size);
   }

   return rtn;
}
