#ifndef MALLOC_H
#define MALLOC_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

#define ALLOC_SIZE 65536
#define BOUNDARY 16
#define BUF_SIZE 100

typedef struct Header {
   long isFree;
   unsigned long size;
   struct Header *next;
   long pad;   //Pad bytes to make Header struct divisible by 16
} Header;

extern Header *head;

//Malloc functions
void *calloc(size_t nmemb, size_t size);
void *malloc(size_t size);
void free(void *ptr);
void *realloc(void *ptr, size_t size);

//Helper functions
Header *FindFree(size_t size);
void Parcel(Header *header, size_t size);
void MergeAdj();

//Debugging function, prints heap layout
void CheckMem();

#endif
