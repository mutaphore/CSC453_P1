#include "malloc.h"

//Walks through linked list and combine adjacent free blocks
void MergeAdj() {
   Header *walker = head;

   while (walker) {
      if (walker->isFree && walker->next && walker->next->isFree) {
         walker->size =
          (char *)walker->next - (char *)walker + walker->next->size;
         walker->next = walker->next->next;
         walker->isFree = 1;
         walker = head;
      }
      else
         walker = walker->next;
   }
}

void free(void *ptr) {
   Header *cur;

	if (ptr) {
   	cur = (Header *)ptr - 1;   //move back to get header info
   	cur->isFree = 1;

   	//Combine adjacent free blocks
   	MergeAdj();

		if (getenv("DEBUG_MALLOC"))
         printf("MALLOC: free(%p)\n", ptr);
   }

}
