#include "malloc.h"

Header *head = NULL;   //Global pointer to head of the linked list of headers

//Cuts and shrinks header to size, sets the next block to free
void Parcel(Header *header, size_t size) {
   Header *temp = (Header *)((char *)(header + 1) + size);
   char *p1, *p2;

   p1 = (char *)temp + sizeof(Header);
   p2 = (char *)(header + 1) + header->size;

   //Make sure enough space to fit new header
   if (p1 < p2) {
      temp->isFree = 1;
      temp->size = p2 - p1;
      temp->next = header->next;
      header->next = temp;
      header->size = size;
      MergeAdj();
   }
}

//Increment program break appropriately and return header
static Header *GetMem(size_t size) {
   Header *rtn = NULL;
   size_t total;

   //Find the minimum allocated memory to accomodate size
   for (total = ALLOC_SIZE; total < size + sizeof(Header);
    total += ALLOC_SIZE)
      ;

   rtn = sbrk(total);
   if (errno == ENOMEM) {
      fprintf(stderr, "%s\n", strerror(errno));
      return NULL;
   }

   rtn->isFree = 0;
   rtn->next = NULL;
   rtn->size = total - sizeof(Header);
   Parcel(rtn, size);

   return rtn;
}

//Traverse through linked list to find suitable free space. If
//nothing is found, allocate more space by incrementing program break.
Header *FindFree(size_t size) {
   Header *walker = head, *prev = NULL;
   int found = 0;

   while (walker && !found) {
      if (walker->isFree && walker->size >= size) {
         found = 1;
         walker->isFree = 0;
         if (walker->size != size)
            Parcel(walker, size);
      }
      else {
         prev = walker;
         walker = walker->next;
      }
   }

   if (!found) {
      if (walker = GetMem(size))
         prev->next = walker;
      else
         return NULL;
   }

   return walker;
}

void *malloc(size_t size) {
   void *rtn = NULL;
   Header *header = NULL;
   size_t inputSize = size;

   if (size > 0) {
      //Check to see if size is divisible by 16
      if (size % BOUNDARY)
         size = (size / BOUNDARY + 1) * BOUNDARY;

      if (head) {   //If linked list is not empty, look for a spot
         if (!(header = FindFree(size)))
            return NULL;
      }
      else {   //If linked list empty, initialize heap space
         if (header = GetMem(size))
            head = header;
         else
            return NULL;
      }
      rtn = header + 1;

      if (getenv("DEBUG_MALLOC")) {
      	printf("MALLOC: malloc(%lu)     =>  (ptr=%p,size=%lu)\n",
          inputSize, rtn, ((Header *)((char *)rtn - sizeof(Header)))->size);
      }
   }
   return rtn;
}

void CheckMem() {
   Header *walker;

   for (walker = head; walker; walker = walker->next)
      printf("isFree = %ld, size = %ld\n", walker->isFree, walker->size);

}
